# Write a method leap_year?. It should accept a year value from the user,
# check whether it's a leap year, and then return true or false.
# With the help of this leap_year?() method calculate and display the number
# of minutes in a leap year (2000 and 2004) and the number of minutes in a
# non-leap year (1900 and 2005). Note: Every year whose number is divisible
# by four without a remainder is a leap year, excepting the full centuries,
# which, to be leap years, must be divisible by 400 without a remainder.
# If not so divisible they are common years. 1900, therefore, is not a leap year.

#doctest: When I enter a 2004 I should get true
#>> leap_year?(2004)
#=> true
#
#doctest: When I enter a 2005 I should get false
#>> leap_year?(2005)
#=> false
#
#doctest: When I enter a 2000 I should get true
#>> leap_year?(2000)
#=> true
#
#doctest: When I enter a 1900 I should get false
#>> leap_year?(1900)
#=> false
#
#doctest: If we send 0 to leap_year? it will raise an error
#>> -> {begin ; leap_year?(0) ; rescue => e ; e.class ; end}.call
#=> RuntimeError
#
#doctest: I can get number of minutes for a leap year
#>> minutes_in_year 2004
#=> 527040
#
#doctest: I can get number of minutes for a non-leap year
#>> minutes_in_year 1999
#=> 525600

def leap_year? year
  raise "0 is not a valid year!" if year.zero?
  year % 4 == 0  && year % 100 != 0 || year % 400 == 0
end

def minutes_in_year year
  60 * 24 * (leap_year?(year) ? 366 :  365)
end

if __FILE__ == $PROGRAM_NAME
  [2004, "2004", 2012, 1999, 1900, 2005].each do |year|
    puts "The year you entered is #{year} and it is a leap year: #{leap_year?(year)}, with #{minutes_in_year(year)} minutes."
  end
end
