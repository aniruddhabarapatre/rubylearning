# Write a Rectangle class. I shall use your class as follows:
# r = Rectangle.new(23.45, 34.67) puts "Area is = #{r.area}"
# puts "Perimeter is = #{r.perimeter}"

class Rectangle
  def initialize(length, breadth)
    @length = length
    @breadth = breadth
  end

  def area
    @length * @breadth
  end

  def perimeter
    2 * (@length + @breadth)
  end
end
