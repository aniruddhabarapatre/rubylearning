# Write a class called Dog, that has name as an instance variable and the following methods:

# bark(), eat(), chase_cat()
# I shall create the Dog object as follows:
# d = Dog.new('Leo')

class Dog
  def initialize(name)
    @name = name
  end

  def bark
    puts "woof"
  end

  def eat
    puts "life is awesome"
  end

  def chase_cat
    puts "my pleasure"
  end
end
