def opt_arg(a, *x, b, name: 'abc')
  puts a.inspect
  puts x.inspect
  puts b.inspect
  puts name.inspect
end

opt_arg(1, 2, 3)
opt_arg(1, 2)
opt_arg(1, 2, 3, 4, 5)
opt_arg(1, 2, :name => 'George')

