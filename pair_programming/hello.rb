# doctest: I can create the world
# >> hello
# => "Hello World!"
# doctest: I can greet someone personally
# >> hello "Sam"
# => "Hello Sam!"
# doctest: I can ask if someone is there
# >> hello('Victor', '?')
# => 'Hello Victor?'
def hello(name = 'World', punctuation = '!')
  "Hello #{name}#{punctuation}"
end
