# Write a method called month_days, that determines the number of days in a month. Usage:
#
# days = month_days(5) # 31 (May)<o:p></o:p>
# days = month_days(2, 2000) # 29 (February 2000)
# Remember, you could use the Date class here. Read the online documentation for the Date class.
# You must account for leap years in this exercise.

# doctest: I pass in month and get number of days
# >> month_days 5
# => 31

# doctest: I pass in month and a non-leap year 
# >> month_days 2, 2001
# => 28
# doctest: I pass in month with a year and get number of days for that month and year
# >> month_days 2, 2000
# => 29
require 'date'

def month_days(month, year = Date.today.year)
  Date.new(year, month, -1).day
end
