#how many hours are in year?

# docTest: I should get hours in an year
# >> hours_in_years
# => 8760

def hours_in_years
  24 * 365
end
