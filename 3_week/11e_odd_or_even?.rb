# Write a Ruby program that, when given an array:

# collection = [12, 23, 456, 123, 4579]
# prints each number, and tells you whether it is odd or even.

def odd_or_even array
  array.each do |num|
    puts (num % 2 == 0) ? "#{num}: even" : "#{num}: odd"
  end
end

collection = [12, 23, 456, 123, 4579]
odd_or_even collection
