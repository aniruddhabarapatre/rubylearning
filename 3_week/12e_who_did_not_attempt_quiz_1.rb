# Something to keep your grey cells ticking.

# I have a database of all the course participants. I want to know the
# number of participants who have not attempted Quiz 1 (is it so scary ?).
# A student writes a clever Ruby program that creates an array of 0's and 1's.
# 0's indicate that the participant has not attempted the Quiz and the 1's have attempted it.

# Use this array quiz:

# quiz = [0,0,0,1,0,0,1,1,0,1]

# and write another clever program to solve my problem. That is display thenumber of participants who have not attempted Quiz 1.

# The output of your program should be as follow:
# The number of participants who did not attempt Quiz 1 is x out of y total participants.

# doctest: I get count of zeros in an array
# >> count_zeros([0,0,1])
# => 2

def count_zeros data
  data.select {|e| e.zero? }.size
end

def count_zeros data
  data.count(0)
end

def count_zeros array
  count = 0
  array.each do |x|
    if x == 0
      count += 1
    end
  end
  count
end

quiz = [0,0,0,1,0,0,1,1,0,1]
puts "The number of participants who did not attempt Quiz 1 is #{count_zeros(quiz)} out of #{quiz.length} total participants."
