# Given the following Ruby code snippet:

a = (1930...1951).to_a
puts a[rand(a.size)]

# When you run this program, which of the following values will not be displayed?
# 1929
# 1930
# 1945
# 1950
# 1951
# 1952
# Explain why that value will not be displayed.

# Also, have a look at the splat operator:

# a = [*1930...1951] # splat operator

=begin
  It will not output 1951 as ... excludes the higher range. So array a will have 21 elements from 1930 to 1950.

  For splat operator when written as arguments for method --

  * at the end - returns an array
  * in between - makes an empty array if not passed

  In the above case we'll have an array with values from 1930 to 1950   
=end