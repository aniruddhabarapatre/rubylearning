# Write a Deaf Grandma program. Whatever you say to grandma (whatever you type in),
# she should respond with HUH?! SPEAK UP, SONNY!, unless you shout it (type in all capitals).
# If you shout, she can hear you (or at least she thinks so) and yells back, NO,
# NOT SINCE 1938! To make your program really believable, have grandma shout a different
# year each time; maybe any year at random between 1930 and 1950.
# You can't stop talking to grandma until you shout BYE.

# Adapted from Chris Pine's Book.

# For example:

# You enter: Hello Grandma
# Grandma responds: HUH?! SPEAK UP, SONNY!
# You enter: HELLO GRANDMA
# Grandma responds: NO, NOT SINCE 1938!

# doctest: I say something to Grandma in lowercase and she asks me to speak up
# >> granny_response "Hello Grandma"
# => "HUH?! SPEAK UP, SONNY!"
#
# I say something to Grandma in upcase and she yells something back with a random year
# doctest: Granny Heard Us!
# >> granny_response("HELLO GRANDMA").include?('NOT SINCE')
# => true

GrannysYears = (1930..1950)
def granny_response shout
  shouted?(shout) ?  "NO, NOT SINCE #{rand(GrannysYears)}!" : "HUH?! SPEAK UP, SONNY!"
end

# Checking for no response by calling empty? method
def shouted?(string)
  string.empty? ? false : string == string.upcase
end

def introduction
  "Granny is hard of hearing, so you must shout ('SHOUT') to be heard.
  shout 'BYE' to leave Granny."
end

if __FILE__ == $0
  puts introduction
  phrase = ''
  until phrase == 'BYE' do
    phrase = gets.chomp
    puts granny_response(phrase)
  end

  puts "Thanks for playing!  Would you like me to run the automated sequence? (enter only to quit)"
  if gets.chomp == ''
    exit
  end

  20.times do
    puts granny_response('THIS SHOULD GIVE A YEAR REMEMBERED')
  end
  ["Something not shouted", "SOMETHING SHOUTED", "BYE", "SOMETHING AGAIN", 'BYE' ].each do |phrase|
    if phrase == 'BYE'
      exit
    else
      puts granny_response(phrase)
    end
  end
end
