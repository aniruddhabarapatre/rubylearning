# Write a Ruby program that, when given an array:
# collection = [1, 2, 3, 4, 5]
# calculates the sum of its elements.

# doctest: I get sum of all elements of an array
# >> sum_array_elements [1, 2, 3, 4, 5]
# => 15

def sum_array_elements array
  sum = 0
  array.each do |x|
    sum = sum + x
  end
  sum
end

puts sum_array_elements [1,2,3]
