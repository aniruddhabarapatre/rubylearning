# Select all answers which return true

h = { "Ruby" => "Matz", "Perl" => "Larry", "Python" => "Guido" }

# Answers:

# member method on hash returns true if key is found
h.member?("Matz") # false
h.member?("Python") # true

# iclude method on hash returns true if key is found
h.include?("Guido") # false
h.include?("Ruby") # true

# has_value checks for value in a given hash and returns true if found
h.has_value?("Larry") #true

puts h.exists?("Perl") # NoMethodError as this doesn't exist for Hash
