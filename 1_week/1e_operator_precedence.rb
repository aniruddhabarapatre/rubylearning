#Discuss your first guess and what you got when running the code.
#Goal: Understanding operator precedence and association.

y = false
z = true
x = y or z
puts x 
# false or true evaluates to true

(x = y) or z
puts x 
# Here we are initializing x with value of y, hence output will be false

x = (y or z)
puts x
# Output is true as (false or true) evalutes to true

# After running the ruby program I got output as -
# false
# false
# true
# which confused me for my 1st answer.
# After going through the forum and looking closely I realised for the
# one I got wrong I evaluated logical condition and then did the assignment.