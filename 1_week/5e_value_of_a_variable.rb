# The following program prints the value of the variable. Why?

my_string = 'Hello Ruby World' 
 
def my_string
    'Hello World' 
end
 
puts my_string

# Will return value Hello Ruby World
# Got confused here as initially I thought it is calling method 
