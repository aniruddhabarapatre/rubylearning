#Read the sprintf documentation and the % documentation in the String class
#and figure out the output being printed by of this Ruby code.

puts "%05d" % 123

# Had no clue about the answer except for the fact that 
# this will be and integer with 0 padding.
# Tried with IRB which evaluated to 00123 and it makes sense.
