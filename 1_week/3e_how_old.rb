# Write a Ruby program that displays how old I am, if I am 979000000 seconds old.
# Display the result as a floating point (decimal) number to two decimal places
# (for example, 17.23).
#
# Note: To format the output to say 2 decimal places, we
# can use the Kernel's format method. For example, if x = 45.5678 then
# format("%.2f", x) will return the string 45.57

# doctest: I can pass in a number of seconds to get age in years
# >> format("%.2f", calculate_years(979000000))
# => "31.04"

def calculate_years seconds
  seconds.to_f / ( 60 * 60 * 24 * 365 )
end

if __FILE__ == $0
  age_in_seconds = 979000000
  puts 'If you are 9.79e8 seconds old you are %.2f years old.' % calculate_years(age_in_seconds)

  [9.79e8, 18.45e8, 6.34e7, 979000000, '9.79e8' ].each do |value|
    puts 'If you are %.2f seconds old, then you are %.2f years old.' % [value, calculate_years(value)]
  end
end
