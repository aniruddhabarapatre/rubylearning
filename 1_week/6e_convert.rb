# Write a method called convert that takes one argument which is a temperature in degrees _Fahrenheit.
# This method should return the temperature in degrees Celsius.
#
# To format the output to say 2 decimal places, we can use the Kernel's format method.
# For example, if x = 45.5678 then format("%.2f", x) will return the string 45.#57.
# Another way is to use the round function as follows:

# doctest: I can pass in a temperature in deg F to get it in deg C
# >> format("%.2f", convert(98))
# => "36.67"

def _Fahrenheit_to_Celsius temp
  (temp - 32.0) * 5 / 9
end

alias :convert :_Fahrenheit_to_Celsius

if __FILE__ == $0
  puts "44 deg F is equal to %.2f deg C" % _Fahrenheit_to_Celsius(44)

  puts "56.2 deg F is equal to %.2f deg C" % _Fahrenheit_to_Celsius(56.2)
end
