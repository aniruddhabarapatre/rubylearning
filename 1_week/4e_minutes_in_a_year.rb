# Write a Ruby program that tells you how many minutes there are in a year

# doctest: I can get number of minutes in a year
# >> cal_min_in_year
# => "525600"

def cal_min_in_year
  puts (60 * 24 * 365)
end

cal_min_in_year
